'use strict'

class ResponseClass {
  constructor(message, status, data, code) {
    this.message = message
    this.status = status
    this.data = data
    this.code = code ? code : null
  }
}

module.exports = ResponseClass
