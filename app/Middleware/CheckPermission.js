'use strict'

const UserService = use('App/Services/User/UserService')
const ErrorException = use('App/Exceptions/ErrorException')

class CheckPermission {
  async handle ({ request, response, auth }, next, schemes) {
    try {
      const acl = schemes
      const user = await auth.current.user
      const aclUser = await UserService.permissionOne(user.id, acl[0], acl[1])

      if (aclUser.data === null || aclUser.data === undefined || aclUser.data === '')
        throw new ErrorException("Você não tem permissão para prosseguir com a solitação", 401)

      await next()

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = CheckPermission
