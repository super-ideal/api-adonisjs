'use strict'

const GE = require('@adonisjs/generic-exceptions')

class ErrorException extends GE.LogicalException {
  /**
   * Handle this exception by itself
   * http://adonisjs.com/docs/4.0/exceptions#_custom_exceptions
   * throw new CustomException(message, status, code)
   */
  async handle (error, { request, response }) {
    switch (error.code) {
      case 'ER_DUP_ENTRY': error.message = 'Os dados informados estão duplicados'; break
      case 'ER_NO_REFERENCED_ROW_2': error.message = 'Alguns dados informados não existe na tabela'; break
      case 'E_MISSING_DATABASE_ROW': error.message = 'Não foi encontrado nenhum resultado com os dados informados'; break
    }

    response.status(error.status).json({
      message: error.message,
      status: error.status,
      code: error.code ? error.code : null,
      name: error.name ? error.name : null
    })
  }
}

module.exports = ErrorException
