'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const OfferMotiveService = use('App/Services/Offer/OfferProductMotiveService')

class OfferMotiveController {
  async index({ request, response }) {
    const result = await OfferMotiveService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await OfferMotiveService.show(params.idMotive)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required'
    }

    const messages = {
      'name.required': 'Um nome deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await OfferMotiveService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await OfferMotiveService.update(data, params.idMotive)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await OfferMotiveService.delete(params.idMotive)
    return response.status(result.status).json(result)
  }
}

module.exports = OfferMotiveController
