'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const { validateAll } = use('Validator')

const PartnerService = use('App/Services/Partner/PartnerService')

class PartnerController {
  async typesPartners ({response}) {
    const result = await PartnerService.typesPartners()
    return response.status(result.status).json(result)
  }

  async index ({request, response}) {
    const result = await PartnerService.index(request.get())
    return response.status(result.status).json(result)
  }

  async show ({params, response}) {
    const result = await PartnerService.show(params.id)
    return response.status(result.status).json(result)
  }

  async store ({request, response, auth}) {
    const data = request.all()
    /**
     * Validation
     * http://indicative.adonisjs.com/#indicative-basics-custom-messages
    */
    const rules = {
      active: 'boolean',
      name_reason: 'required',
      cpf_cnpj: 'unique:partners',
      rg_ie: 'unique:partners'
    }

    const messages = {
      'active.boolean': 'O campo Active não é um campo booleano',
      'name_reason.required': 'Você deve informar o nome ou razão social',
      'cpf_cnpj.unique': 'O CPF/CNPJ informado já existe na base de dados',
      'rg_ie.unique': 'O RG/IE informado já existe na base de dados',
    }

    const validation = await validateAll(data.partner, rules, messages)
    if (validation.fails()) throw new ErrorException(validation.messages(), 422)

    // Salva
    const result = await PartnerService.store(data, auth)
    return response.status(result.status).json(result)
  }

  async update ({params, request, response, auth}) {
    const data = request.all()

    const rules = {
      active: 'boolean'
    }

    const messages = {
      'active.boolean': 'O campo active não é um campo booleano'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Atualiza
    const result = await PartnerService.update(data, params.id, auth)
    return response.status(result.status).json(result)
  }
}

module.exports = PartnerController
