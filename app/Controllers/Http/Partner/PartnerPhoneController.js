'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const PartnerPhoneService = use('App/Services/Partner/PartnerPhoneService')

class PartnerPhoneController {
  async index({ request, response }) {
    const result = await PartnerPhoneService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await PartnerPhoneService.show(params.id)
    return response.status(result.status).json(result)
  }

  async store({ request, response, params }) {
    const result = await PartnerPhoneService.store(request.post(), params.idPartner)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await PartnerPhoneService.update(data, params.id, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await PartnerPhoneService.delete(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = PartnerPhoneController
