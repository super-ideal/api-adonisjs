'use strict'
const Validator = require('./validator/rule-where-by');

class IhxAcessoController {
  async index ({request, response}) {
    const Acesso = use('App/Models/ihx/Acesso')

    const params = request.all()

    const page = params.page ? parseInt(params.page, 10) : 1
    const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
    const whereBy = Validator.ruleWhereBy(params.whereBy) ? params.whereBy : 'id'
    const whereText = params.whereText ? params.whereText : ''

    const orderBy = 'acessodatahora';
    const orderSort = 'desc';

    const acessos = await Acesso
      .query()
      .whereRaw(whereBy + ' like ? ', ['%'+whereText+'%'])
      .orderBy(orderBy, orderSort)
      .paginate(page, perPage)

    return response.json(acessos)
  }
}

module.exports = IhxAcessoController
