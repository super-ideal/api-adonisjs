'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const AttributesTypeService = use('App/Services/Product/Attributes/AttributesTypeService')

class AttributesTypeController {
  async index({ request, response }) {
    const result = await AttributesTypeService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await AttributesTypeService.show(params.idType)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required',
      style_id: 'required'
    }

    const messages = {
      'name.required': 'Um Nome deve ser informado',
      'style_id.required': 'O Estilo deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await AttributesTypeService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await AttributesTypeService.update(data, params.idType)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await AttributesTypeService.delete(params.idType)
    return response.status(result.status).json(result)
  }
}

module.exports = AttributesTypeController
