'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const SectionTypeService = use('App/Services/Product/Section/SectionTypeService')

class SectionTypeController {
  async index({ request, response }) {
    const result = await SectionTypeService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, request, response }) {
    const result = await SectionTypeService.show(params.idType, request.all())
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required'
    }

    const messages = {
      'name.required': 'Um Nome deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await SectionTypeService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await SectionTypeService.update(data, params.idType)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await SectionTypeService.delete(params.idType)
    return response.status(result.status).json(result)
  }
}

module.exports = SectionTypeController
