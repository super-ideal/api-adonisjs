'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const ImageService = use('App/Services/Product/ImageService')

class ImageController {
  async index({ params, request, response }) {
    const result = await ImageService.index(request.all(), params.idProduct)
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await ImageService.show(params.idImage)
    return response.status(result.status).json(result)
  }

  async store({ params, request, response }) {
    const data = request.all()

    const rules = {
      filename: 'required|unique:product_images',
      fieldname: 'required'
    }

    const messages = {
      'filename.required': 'O nome do arquivo deve ser informado',
      'filename.unique': 'O nome do arquivo deve ser unico',
      'fieldname.required': 'O nome da pasta do arquivo deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await ImageService.store(data, params.idProduct)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await ImageService.update(data, params.idImage)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await ImageService.delete(params.idImage)
    return response.status(result.status).json(result)
  }
}

module.exports = ImageController
