'use strict'
const ProductService = use('App/Services/Product/Public/ProductService')

class ProductController {
  async index({ request, response }) {
    const result = await ProductService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await ProductService.show(params.idProduct)
    return response.status(result.status).json(result)
  }

  async indexProductsSection({ request, params, response }) {
    const result = await ProductService.indexProductsSection(params.idSection, request.all())
    return response.status(result.status).json(result)
  }
}

module.exports = ProductController
