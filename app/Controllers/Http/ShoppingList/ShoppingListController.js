'use strict'
const ShoppingListService = use('App/Services/ShoppingList/ShoppingListsService')

class ShoppingListController {
  async index({ request, response }) {
    const result = await ShoppingListService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await ShoppingListService.show(params.idList)
    return response.status(result.status).json(result)
  }
}

module.exports = ShoppingListController
