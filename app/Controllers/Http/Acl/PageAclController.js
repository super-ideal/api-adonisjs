'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const PageService = use('App/Services/Acl/PageService')

class PageAclController {
  async storePage({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required|max:80|unique:acl_pages',
      description: 'max:255',
    }

    const messages = {
      'name.required': 'Você deve informar o nome da Página',
      'name.max': 'A Página informada é muito grande',
      'name.unique': 'A Página informada já está cadastrada',
      'description.max': 'A descrição informada é muito grande',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await PageService.store(data)
    return response.status(result.status).json(result)
  }

  async indexPage({ response }) {
    const result = await PageService.index()
    return response.status(result.status).json(result)
  }

  async deletePage({ params, response }) {
    const result = await PageService.delete(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = PageAclController
