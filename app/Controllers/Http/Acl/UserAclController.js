'use strict'
const UserAclService = use('App/Services/Acl/UserAclService')

class UserAclController {
  async showPermissionsUserId({ params, response }) {
    const result = await UserAclService.showPermissionsUserId(params.idUser)
    return response.status(result.status).json(result)
  }

  async showPermissionsUser({ response, auth }) {
    const result = await UserAclService.showPermissionsUser(auth)
    return response.status(result.status).json(result)
  }

  async update({ response, request, params, auth }) {
    const result = await UserAclService.update(request.all(), params.idUser, auth)
    return response.status(result.status).json(result)
  }
}

module.exports = UserAclController
