'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const PageActionService = use('App/Services/Acl/PageActionService')

class PageActionAclController {
  async storeActionPage ({ request, response }) {
    const data = request.all()

    const rules = {
      page_id: 'required',
      action_id: 'required',
    }

    const messages = {
      'page_id.required': 'Você deve informar o ID da página',
      'action_id.required': 'Você deve informar o ID da ação',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await PageActionService.storeActionPage(data)
    return response.status(result.status).json(result)
  }

  async allPageAction ({ response }) {
    const result = await PageActionService.allPageAction()
    return response.status(result.status).json(result)
  }

  async showActionsPage ({ params, response }) {
    const result = await PageActionService.showActionsPage(params.idPage)
    return response.status(result.status).json(result)
  }

  async deleteActionPage({ params, response }) {
    const result = await PageActionService.deleteActionPage(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = PageActionAclController
