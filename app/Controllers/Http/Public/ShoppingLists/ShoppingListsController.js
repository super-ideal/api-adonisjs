'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const { validateAll } = use('Validator')

const ShoppingListsService = use('App/Services/Public/ShoppingLists/ShoppingListsService')

class ShoppingListsController {
  async getAllPublic({ request, response }) {
    const result = await ShoppingListsService.getAllPublic(request.all())
    return response.status(result.status).json(result)
  }

  async copyListNewUser({ params, response, auth }) {
    const result = await ShoppingListsService.copyListNewUser(params.idList, auth)
    return response.status(result.status).json(result)
  }

  async index({ request, response, auth }) {
    const result = await ShoppingListsService.index(request.all(), auth)
    return response.status(result.status).json(result)
  }

  async show({ params, response, auth }) {
    const result = await ShoppingListsService.show(params.idList, auth)
    return response.status(result.status).json(result)
  }

  async showListShared({ params, response }) {
    const result = await ShoppingListsService.showListShared(params.idList)
    return response.status(result.status).json(result)
  }

  async store({ request, response, auth }) {
    const data = request.all()

    const rules = {
      name: 'required|max:50',
      public: 'boolean',
      shared: 'boolean'
    }

    const messages = {
      'name.required': 'O nome da lista deve ser informado',
      'name.max': 'O tamanho máximo permitido do nome da lista é de 50 caracteres',
      'shared.boolean': 'O atributo shared não é do tipo booleano',
      'public.boolean': 'O atributo public não é do tipo booleano'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ErrorException(validation.messages(), 422)

    const result = await ShoppingListsService.store(data, auth)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await ShoppingListsService.update(data, params.idList, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response, auth }) {
    const result = await ShoppingListsService.delete(params.idList, auth)
    return response.status(result.status).json(result)
  }
}

module.exports = ShoppingListsController
