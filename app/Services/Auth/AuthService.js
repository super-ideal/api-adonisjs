'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const { validateAll } = use('Validator')
const ValidationException = use('App/Exceptions/ValidationException')

const UserView = use('App/Models/User/UserView')
const Partner = use('App/Models/Partner/Partner')
const PageActionUserView = use('App/Models/Acl/PageActionUserView')

class AuthService {

  async login (auth, data) {
    try {
      const email = data && data.email ? data.email : null
      const cpf_cnpj = data && data.cpf_cnpj ? data.cpf_cnpj : null
      let token;

      if (email) {
        const rules = {
          email: 'required|email',
          password: 'required'
        }

        const messages = {
          'email.required': 'O email é necessário',
          'email.email': 'O email informado não é válido',
          'password.required': 'Uma senha deve ser informada',
        }

        const validation = await validateAll(data, rules, messages)
        if (validation.fails()) throw new ValidationException(validation.messages())

        token = await auth.authenticator('jwt_email').withRefreshToken().attempt(email, data.password)

      } else if (cpf_cnpj) {
        const rules = {
          cpf_cnpj: 'required',
          password: 'required'
        }

        const messages = {
          'cpf_cnpj.required': 'O CPF/CNPJ é necessário',
          'password.required': 'Uma senha deve ser informada',
        }

        const validation = await validateAll(data, rules, messages)
        if (validation.fails()) throw new ValidationException(validation.messages())

        token = await auth.authenticator('jwt_cpfcnpj').withRefreshToken().attempt(cpf_cnpj.toString().trim(), data.password.toString().trim())
      } else {
        throw new ErrorException('Email ou CPF/CNPJ deve ser informados', 500)
      }

      const user = await UserView
        .query()
        .whereRaw('email = ? or cpf_cnpj = ?', [email, cpf_cnpj])
        .with('permissions')
        .first()

      let result = {
        user: user,
        auth: token
      }

      return new Response('Login realizado com sucesso', 200, result)

    } catch (error) {
      switch (error.name) {
        case 'UserNotFoundException': error.message = 'E-mail ou senha inválidos'; break
        case 'PasswordMisMatchException': error.message = 'E-mail ou senha inválidos'; break
      }

      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async register(data, auth) {
    try {
      // Cadastra a pessoa
      const partner = await Partner.create(data.partner)

      // Cadastra o usuário
      delete data.user['status'] // previne que o usuário não esteja passando um status de admin por exemplo
      await partner.user().create(data.user)

      // Faz o login do usuário
      let tokenLogin = await this.login(auth, {password: data.user.password, email: data.user.email, cpf_cnpj: data.partner.cpf_cnpj})
      return new Response("Usuário cadastrado com sucesso!", 200, tokenLogin['data'])

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async refreshToken (auth, refresh_token) {
    try {
      const data = await auth.generateForRefreshToken(refresh_token)
      return new Response('Novo token gerado com sucesso', 200, data)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async logoutAll (auth) {
    try {
      const user = auth.current.user
      await user
        .tokens()
        .where('type', 'jwt_refresh_token')
        .delete()

      return new Response('Você saiu do sistema com sucesso', 200)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async getTokens(auth) {
    try {
      const tokens = await auth.listTokens()
      return new Response('', 200, tokens)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async getUserLogged(auth) {
    try {
      const user = await auth.getUser()
      const permissions = await PageActionUserView.query().where('user_id', user.id).fetch()
      user['permissions'] = permissions;

      return new Response('', 200, user)

    } catch (error) {
      throw new ErrorException('Ocorreu um erro ao buscar os dados do usuário', error.status, error.code)
    }
  }

  async getName(data) {
    try {
      data.email = data.email || ''
      data.cpf_cnpj = data.cpf_cnpj || ''

      const result = await UserView
      .query()
      .select('name_reason')
      .whereRaw('email = ? or cpf_cnpj = ?', [data.email, data.cpf_cnpj])
      .first()

      if (result !== null && result !== undefined) {
        const obj = {};

        if (result.name_reason.trim().split(' ')[1] != undefined) {
          obj['name_reason'] = result.name_reason.trim().split(' ')[0] + ' ' + result.name_reason.trim().split(' ')[1]
        } else {
          obj['name_reason'] = result.name_reason.trim().split(' ')[0]
        }

        return new Response('', 200, result)

      } else {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }
    } catch (error) {
      throw new ErrorException('Ocorreu um erro ao buscar os dados do usuário', error.status, error.code)
    }
  }
}

module.exports = new AuthService()
