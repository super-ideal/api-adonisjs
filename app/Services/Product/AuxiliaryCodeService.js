'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Product = use('App/Models/Product/Product')
const AuxiliaryCode = use('App/Models/Product/AuxiliaryCode')

class AuxiliaryCodeService {
  async getAll(product_id) {
    try {
      const result = await AuxiliaryCode
        .query()
        .whereRaw('product_id = ?', [product_id])
        .with('product', (builder) => {
          builder.select('id', 'description', 'barcode', 'active')
        })
        .fetch()

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async getCode(code) {
    try {
      const result = await AuxiliaryCode
        .query()
        .whereRaw('code = ?', [code])
        .with('product', (builder) => {
          builder.select('id', 'description', 'barcode', 'active')
        })
        .first()

      //const result = await AuxiliaryCode.findByOrFail({code: code})
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const barcodeProduct = await Product.findBy('barcode', data.code)

      if (barcodeProduct != null) {
        throw new ErrorException('O código informado já esta cadastrado em um produto', 500, 'ER_DUP_ENTRY', 'ErrorException')
      }

      const result = await AuxiliaryCode.create(data)
      return new Response('Código auxiliar CADASTRADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const barcodeProduct = await Product.findBy('barcode', data.code)

      if (barcodeProduct != null) {
        throw new ErrorException('O código informado já esta cadastrado em um produto', 500, 'ER_DUP_ENTRY', 'ErrorException')
      }

      const result = await AuxiliaryCode.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response('Código auxiliar ATUALIZADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await AuxiliaryCode.findOrFail(id)
      await result.delete()

      return new Response('Código auxiliar EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new AuxiliaryCodeService()
