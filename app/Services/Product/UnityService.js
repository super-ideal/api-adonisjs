'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Unity = use('App/Models/Product/Unity')

class UnityService {
  async index() {
    try {
      const unitys = await Unity.query().fetch()
      return new Response(null, 200, unitys)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new UnityService()
