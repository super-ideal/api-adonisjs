'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const Attributes = use('App/Models/Product/Attributes/Attributes')

class AttributesService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'name'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'name'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let active = params.active ? params.active : null
      if (active == 'true') { active = 1 } else if (active == 'false') { active = 0 }

      const result = await Database
        .table('product_attributes_type_view')
        .whereRaw(wBy + ' like ?', ['%'+w+'%'])
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .orderBy(oBy, oSort)
        .paginate(page, perPage)

      if (result.data.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      /*const result = await Attributes
        .query()
        .with('type', (builder) => {
          builder.select('id', 'name', 'description', 'active', 'multiple')
        })
        .whereRaw(wBy + ' like ? ', ['%'+w+'%'])
        .orderBy(oBy, oSort)
        .paginate(page, perPage)*/

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async searchAttributesByTypes(idType) {
    try {
      const result = await Attributes
      .query()
      .select('id', 'name')
      .whereRaw('type_id = ? and active = 1', [idType])
      .fetch()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Attributes
      .query()
      .whereRaw('id = ? ', [id])
      .with('type', (builder) => {
        builder.select('id', 'name', 'description', 'active', 'required', 'style_id')
        .with('style')
      })
      .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await Attributes.create(data)
      return new Response('Atributo CADASTRADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const result = await Attributes.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response('Atributo ATUALIZADO com sucesso!', 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await Attributes.findOrFail(id)
      await result.delete()

      return new Response('Atributo EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new AttributesService()
