'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const AttributesType = use('App/Models/Product/Attributes/AttributesType')

class AttributesTypeService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'name'
      const w = params.w ? params.w : ''
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const active = params.active ? (JSON.parse(params.active) == null ? null : +JSON.parse(params.active)) : null
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false
      params.detailsAttributes = params.detailsAttributes ? JSON.parse(params.detailsAttributes) : false

      let query = AttributesType.query().whereRaw(wBy + ' like ?', ['%' + w + '%']).with('style')
      if (active != null)
        query = query.whereRaw('active = ?', [active])

      if (params.detailsAttributes) {
        query = query.with('attributes', (builder) => {
          builder.where('active', true)
          builder.select('id', 'name', 'active', 'type_id')
        })
      }

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.length === 0)
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await AttributesType
      .query()
      .whereRaw('id = ?', [id])
      .with('style')
      .first()

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await AttributesType.create(data)
      return new Response('Tipo de atributo CADASTRADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const result = await AttributesType.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response("Tipo de atributo ATUALIZADO com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await AttributesType.findOrFail(id)
      await result.delete()

      return new Response('Tipo de atributo EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new AttributesTypeService()
