'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const OfferType = use('App/Models/Offer/OfferType')

class OfferTypeService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'name'
      const w = params.w ? params.w : ''
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const active = params.active ? (JSON.parse(params.active) == null ? null : +JSON.parse(params.active)) : null
      const pagination = params.pagination ? JSON.parse(params.pagination) : false

      let query = OfferType.query().whereRaw(wBy + ' like ?', ['%' + w + '%'])
      if (active != null)
        query = query.whereRaw('active = ?', [active])

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.length === 0)
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')

      return new Response(null, 200, result)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await OfferType.findOrFail(id)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await OfferType.create(data)
      return new Response('Tipo de oferta CADASTRADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const result = await OfferType.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response("Tipo de oferta ATUALIZADO com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await OfferType.findOrFail(id)
      await result.delete()

      return new Response('Tipo de oferta EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new OfferTypeService()
