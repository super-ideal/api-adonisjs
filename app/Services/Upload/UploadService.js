'use strict'

const Env = use('Env')
const Cloudinary = require('cloudinary');

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

class UploadService {

  constructor() {
    Cloudinary.config({
      cloud_name: Env.get('CLOUDINARY_NAME', ''),
      api_key: Env.get('CLOUDINARY_API_KEY', ''),
      api_secret: Env.get('CLOUDINARY_API_SECRET', '')
    });
  }

  async cloudinary(data) {
    return await Cloudinary.v2.uploader.upload(data.tmpPath, {
      use_filename: false,
      chunk_size: 10000,
      resource_type: "image"
    });
  }

  async upload (images) {
    try {
      let data;

      if (images.files){
        let upload_len = images.files.length, upload_res = new Array();
        for(let i = 0; i < upload_len; i++)
        {
          let image = images.files[i];
          let cloudinaryMeta = await this.cloudinary(image);

          upload_res.push(cloudinaryMeta);
        }
        data = upload_res;
      } else {
        data = await this.cloudinary(images);
      }

      return new Response('Upload realizado com sucesso!', 200, data)

    } catch (error) {
      switch (error.name) {
        case 'UserNotFoundException': error.message = 'E-mail ou senha inválidos'; break
        case 'PasswordMisMatchException': error.message = 'E-mail ou senha inválidos'; break
      }

      throw new ErrorException(error.message, error.status, error.code)
    }
  }

}

module.exports = new UploadService()
