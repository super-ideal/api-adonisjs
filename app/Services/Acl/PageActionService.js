'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const PageAction = use('App/Models/Acl/PageAction')
const PageActionView = use('App/Models/Acl/PageActionView')
const Page = use('App/Models/Acl/Page')

class PageActionService {
  /**
   * Grava uma ou mais ações em uma página
   * @param {*} data Array com o id da página e o id da ação
   */
  async storeActionPage(data) {
    try {
      const result = await PageAction.create(data)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todas as páginas e ações
   */
  async allPageAction() {
    try {
      const pages = await PageActionView.query().orderBy('page_level', 'desc').fetch()

      if (pages.length == 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, pages)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todas as ações que uma página tem
   * @param {*} idPage
   */
  async showActionsPage(idPage) {
    try {
      const page = await Page
        .query()
        .where('id', idPage)
        .select('id', 'name', 'description')
        .with('actions', (builder) => {
          builder.select('id', 'name', 'description')
        })
        .fetch()

      if (!page) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, page)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui uma ActionPage
   * @param {*} id Id único da ActionPage
   */
  async deleteActionPage(id) {
    try {
      const result = await PageAction.findOrFail(id)
      await result.delete()

      return new Response('ActionPage EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new PageActionService()
