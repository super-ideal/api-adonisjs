'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const ShoppingLists = use('App/Models/Public/ShoppingLists/ShoppingLists')
const ShoppingListsView = use('App/Models/Public/ShoppingLists/ShoppingListsView')
const ShoppingListProducts = use('App/Models/Public/ShoppingLists/ShoppingListProducts')

class ShoppingListsService {
  async getAllPublic(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : true
      params.listProducts = params.listProducts ? JSON.parse(params.listProducts) : true

      let query = ShoppingListsView
        .query()
        .where({'public': 1})
        .whereRaw(wBy + ' like ? ', ['%' + w + '%'])

      if (params.listProducts) {
        query = query.with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.select('id', 'barcode', 'description')
              .with('unity')
              .with('images', (builder) => {
                builder.select('product_id', 'id', 'filename')
              })
          })
        })
      }

      query = query.orderBy(oBy, oSort)

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async index(params, auth) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : true
      params.listProducts = params.listProducts ? JSON.parse(params.listProducts) : true

      const user = await auth.getUser()

      let query = ShoppingListsView
        .query()
        .where({'user_id': user.id})
        .whereRaw(wBy + ' like ? ', ['%' + w + '%'])

      if (params.listProducts) {
        query = query.with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.select('id', 'barcode', 'description')
              .with('unity')
              .with('images', (builder) => {
                builder.select('product_id', 'id', 'filename')
              })
          })
        })
      }

      query = query.orderBy(oBy, oSort)

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async showListShared(idList) {
    try {
      const result = await ShoppingListsView
        .query()
        .where({ 'shared': 1, 'id': idList })
        .with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.select('id', 'barcode', 'description')
              .with('images', (builder) => {
                builder.select('product_id', 'id', 'filename', 'fieldname')
              })
          })
        })
        .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(idList, auth) {
    try {
      const user = await auth.getUser()

      let query = ShoppingListsView
        .query()
        .where({ 'shopping_lists_view.user_id': user.id, 'shopping_lists_view.id': idList })
        .with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.select('id', 'barcode', 'description')
              .with('unity')
              .with('images', (builder) => {
                builder.select('product_id', 'id', 'filename')
              })
          })
        })
        .first()

      const result = await query

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(body, auth) {
    try {
      const user = await auth.getUser()
      body['user_id'] = user.id

      const data = await ShoppingLists.create(body)
      return new Response('Lista de compra cadastrada com sucesso', 200, data)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(body, idList, auth) {
    try {      
      const data = await ShoppingLists.findOrFail(idList)
      const user = await auth.getUser()

      if (data.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para atualizar a lista de compras', 401)
      }

      data.merge(body)
      await data.save()

      return new Response('Dados atualizados com sucesso', 200, data)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui uma lista
   * @param {*} id Id único da lista
   */
  async delete(idList, auth) {
    try {
      const result = await ShoppingLists.findOrFail(idList)
      const user = await auth.getUser()

      if (result.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para excluir a lista de compras', 401)
      }

      await result.listProducts().delete()
      await result.delete()

      return new Response('Lista de compra EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async copyListNewUser(idList, auth) {
    try {
      const user = await auth.getUser()

      // Busca a lista antiga e cadastra a nova lista
      let list = await ShoppingLists.findOrFail(idList);
      const listJson = list.toJSON()

      listJson['user_id'] = user.id
      listJson['shared'] = 0
      listJson['public'] = 0
      delete listJson['created_at'];
      delete listJson['updated_at'];
      delete listJson['id'];
      const newList = await ShoppingLists.create(listJson)

      // Busca os produtos da lista antiga e cadastra na nova lista
      const productsList = await ShoppingListProducts.query().where({ 'list_id': idList }).fetch()
      const productsListJson = productsList.toJSON()
      
      let products = [];
      productsListJson.forEach((element, index) => {
        delete element['created_at'];
        delete element['updated_at'];
        delete element['id'];
        element['list_id'] = newList.id

        products.push(element);
      });
      const newProductsList = await newList.listProducts().createMany(products)

      // Faz tratamento para mostrar nos novos dados
      const result = newList.toJSON();
      result['listProducts'] = newProductsList

      return new Response('Lista de compra cadastrada com sucesso', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ShoppingListsService()
