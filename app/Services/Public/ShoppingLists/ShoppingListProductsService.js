'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const ShoppingLists = use('App/Models/Public/ShoppingLists/ShoppingListsView')
const ShoppingListProducts = use('App/Models/Public/ShoppingLists/ShoppingListProducts')

class ShoppingListProductsService {
  async getAllPublic(idList, params) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false

      const list = await ShoppingLists.findOrFail(idList)

      if (list.public == 0) {
        throw new ErrorException('O usuário não tem permissão para buscar os produtos da lista de compra', 401)
      }

      let query = ShoppingListProducts
        .query()
        .where({'shopping_list_products.list_id': idList })
        .whereRaw('shopping_list_products.' + wBy + ' like ? ', ['%' + w + '%'])
        .with('product', (builder) => {
          builder.select('id', 'barcode', 'description')
            .with('unity')
            .with('images')
        })
        .orderBy(oBy, oSort)

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async index(idList, params, auth) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false

      const list = await ShoppingLists.findOrFail(idList)
      const user = await auth.getUser()

      if (list.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para buscar os produtos da lista de compra', 401)
      }

      let query = ShoppingListProducts
        .query()
        .where({'shopping_list_products.list_id': idList })
        .whereRaw('shopping_list_products.' + wBy + ' like ? ', ['%' + w + '%'])
        .with('product', (builder) => {
          builder.select('id', 'barcode', 'description')
            .with('unity')
            .with('images')
        })
        .orderBy(oBy, oSort)

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(body, auth) {
    try {
      const list = await ShoppingLists.findOrFail(body.list_id)
      const user = await auth.getUser()

      if (list.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para adicionar produtos a lista de compras', 401)
      }

      const result = await list.listProducts().create(body)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(body, idList, idListProduct, auth) {
    try {
      const list = await ShoppingLists.findOrFail(idList)
      const user = await auth.getUser()

      if (list.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para atualizar um produto da lista de compras', 401)
      }

      const result = await ShoppingListProducts.findOrFail(idListProduct)
      result.merge(body)
      await result.save()

      return new Response('Dados atualizados com sucesso', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui uma lista
   * @param {*} idList Id único da lista
   */
  async delete(idList, idListProduct, auth) {
    try {
      const list = await ShoppingLists.findOrFail(idList)
      const user = await auth.getUser()

      if (list.user_id !== user.id) {
        throw new ErrorException('O usuário não tem permissão para excluir um produto da lista de compras', 401)
      }

      const result = await ShoppingListProducts.findOrFail(idListProduct)
      await result.delete()

      return new Response('Produto EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ShoppingListProductsService()
