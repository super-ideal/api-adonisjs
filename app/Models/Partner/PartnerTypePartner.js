'use strict'

const Model = use('Model')

class PartnerTypePartner extends Model {
  static get table () {
    return 'partner_types_partner'
  }

  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }
}

module.exports = PartnerTypePartner
