'use strict'

const Model = use('Model')

class SectionType extends Model {
  static get table () {
    return 'product_section_types'
  }

  getRequired (required) {
    return required == 1 ? true : false
  }

  setRequired (required) {
    return (required == true || required == 'true' || required == 1) ? 1 : 0
  }

  getEditable (editable) {
    return editable === 1 ? true : false
  }

  setEditable (editable) {
    return (editable == true || editable == 'true' || editable == 1) ? 1 : 0
  }

  parent() {
    return this.belongsTo('App/Models/Product/Section/SectionType', 'parent_id', 'id')
  }

  // ADDRESSTYPE -> Section (Tabela Seção recebe mais de um Tipo de Seção)
  section() {
    return this.hasMany('App/Models/Product/Section/Section', 'id', 'type_id')
  }
}

module.exports = SectionType
