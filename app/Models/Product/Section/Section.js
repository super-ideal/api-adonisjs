'use strict'

const Model = use('Model')

class Section extends Model {
  static get table () {
    return 'product_section'
  }

  getActive(active) {
    return active == 1 ? true : false
  }

  setActive(active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  products() {
    return this
      .belongsToMany('App/Models/Product/Product')
      .pivotModel('App/Models/Product/Section/SectionProduct')
  }

  parent() {
    return this.belongsTo('App/Models/Product/Section/Section', 'parent_id', 'id')
  }

  // SectionType -> ADDRESS
  type () {
    return this.belongsTo('App/Models/Product/Section/SectionType', 'type_id')
  }
}

module.exports = Section
