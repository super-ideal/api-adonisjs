'use strict'

const Model = use('Model')

class AttributesProduct extends Model {
  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }
  
  static get table () {
    return 'product_attributes_products'
  }
}

module.exports = AttributesProduct
