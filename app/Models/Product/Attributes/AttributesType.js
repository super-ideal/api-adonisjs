'use strict'

const Model = use('Model')

class Type extends Model {
  static get table () {
    return 'product_attributes_types'
  }

  getActive (active) {
    return active == 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  getRequired (required) {
    return required == 1 ? true : false
  }

  setRequired (required) {
    return (required == true || required == 'true' || required == 1) ? 1 : 0
  }

  getEditable (editable) {
    return editable === 1 ? true : false
  }

  setEditable (editable) {
    return (editable == true || editable == 'true' || editable == 1) ? 1 : 0
  }

  style() {
    return this.belongsTo('App/Models/Product/Attributes/AttributesTypeStyle', 'style_id')
  }

  // AttributesType -> Attributes (Tabela Attributo recebe mais de um Tipo de Atributo)
  attributes () {
    return this.hasMany('App/Models/Product/Attributes/Attributes')
  }
}

module.exports = Type
