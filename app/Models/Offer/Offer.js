'use strict'

const Model = use('Model')

class Offer extends Model {
  static get table () {
    return 'offers'
  }

  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  // OFFER -> OfferProduct
  offerProduct () {
    return this.hasMany('App/Models/Offer/OfferProduct')
  }

  // OfferType -> OFFER
  offerType () {
    return this.belongsTo('App/Models/Offer/OfferType', 'offer_types_id')
  }

  // User -> OFFER
  created () {
    return this.belongsTo('App/Models/User/User', 'created_by')
  }

  // User -> OFFER
  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }
}

module.exports = Offer
