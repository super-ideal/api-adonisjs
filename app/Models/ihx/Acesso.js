'use strict'

const Model = use('Model')

class IhxAcesso extends Model {
  static boot () {
    super.boot()
  }

  static get connection () {
    return 'ihx'
  }

  static get table () {
    return 'acesso'
  }

  static get createdAtColumn () {
    return 'acessodatahora'
  }

  static get updatedAtColumn () {
    return null
  }

  static get visible () {
    return ['id', 'acessodatahora', 'acessoliberado', 'acessodescricao', 'acessosentido', 'cracha', 'equipamentodescricao']
  }

  /**
   * Getter
   * @param {*} acessoliberado
   */
  getAcessoliberado (acessoliberado) {
    return acessoliberado === 1 ? 'Sim' : 'Não'
  }
}

module.exports = IhxAcesso
