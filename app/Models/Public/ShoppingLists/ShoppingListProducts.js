'use strict'

const Model = use('Model')

class ShoppingListProducts extends Model {
  static get table() {
    return 'shopping_list_products'
  }

  product() {
    return this.hasOne('App/Models/Product/Product', 'product_id', 'id')
  }

  list() {
    return this.hasOne('App/Models/Public/ShoppingLists/ShoppingListsView')
  }
}

module.exports = ShoppingListProducts
