'use strict'

const Model = use('Model')

class Page extends Model {
  static get table () {
    return 'acl_pages'
  }

  actions() {
    return this
      .belongsToMany('App/Models/Acl/Action')
      .pivotModel('App/Models/Acl/PageAction')
      .withPivot(['id'])
  }

  pageActionUser() {
    return this.manyThrough('App/Models/Acl/PageAction', 'pageActionUser')
  }
}

module.exports = Page
