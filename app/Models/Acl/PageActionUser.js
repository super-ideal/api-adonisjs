'use strict'

const Model = use('Model')

// PivotModel
class PageActionUser extends Model {
  static get table() {
    return 'acl_page_action_users'
  }

  static get createdAtColumn() {
    return null
  }

  static get updatedAtColumn() {
    return null
  }
}

module.exports = PageActionUser
