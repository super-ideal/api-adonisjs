CREATE OR REPLACE VIEW users_view AS
SELECT u.active, u.username, u.email, u.status, u.partner_id, u.id, u.password, u.created_at, u.updated_at,
p.active as partner_active, p.name_reason, p.name_fantasy, p.cpf_cnpj, p.rg_uf, p.rg_issuing_body, p.rg_ie, p.type_person, p.sex
FROM users AS u
LEFT JOIN partners as p on p.id = u.partner_id
