/*
Mostra todas as agendas de ofertas
*/

CREATE OR REPLACE VIEW offer_view AS
SELECT o.*, pcreated.name_reason 'created_name_reason', pupdated.name_reason 'updated_name_reason',
ot.name 'offer_type_name', ot.active 'offer_type_active', ot.opt_top_offer, ot.opt_discount, ot.opt_discount_price_two, ot.opt_cumulative, ot.opt_cumulative_payable, ot.opt_wholesale, ot.opt_price_group, ot.opt_limit_per_coupon, ot.opt_view_percent_discount,
(CASE
	WHEN o.active = 0 THEN 'DESATIVADA'
	WHEN o.datetime_start <= NOW() && o.datetime_end >= NOW() THEN 'ANDAMENTO'
	WHEN o.datetime_start > NOW() THEN 'AGUARDANDO'
	ELSE 'FINALIZADA'
END) AS status, now() as date_and_hour_database
from offers as o
inner join offer_types as ot on ot.id = o.offer_types_id
left join users as ucreated on ucreated.id = o.created_by
left join partners as pcreated on pcreated.id = (select id from users where id = ucreated.partner_id)
left join users as uupdated on uupdated.id = o.updated_by
left join partners as pupdated on pupdated.id = (select id from users where id = uupdated.partner_id)
