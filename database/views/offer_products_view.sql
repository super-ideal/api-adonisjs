CREATE OR REPLACE VIEW offer_products_view AS
SELECT op.*, om.name AS offer_motive_name,
p.barcode 'product_barcode', p.description 'product_description', p.price 'product_price', p.active 'product_active',
padded.name_reason AS added_by_name_reason, pupdated.name_reason AS updated_by_name_reason,
(CASE
	WHEN discount_price_two IS NOT NULL AND discount IS NOT NULL AND cumulative IS NOT NULL AND cumulative_payable IS NULL THEN 'PREÇO DOIS ACUMULATIVO'
	WHEN discount_price_two IS NOT NULL AND discount IS NOT NULL THEN 'PREÇO DOIS'
	WHEN discount IS NOT NULL AND cumulative IS NOT NULL AND cumulative_payable IS NULL THEN 'DESCONTO ACUMULATIVO'
	WHEN discount IS NOT NULL AND discount_price_two IS NULL THEN 'DESCONTO'
	WHEN cumulative IS NOT NULL AND cumulative_payable IS NOT NULL THEN 'LEVE MAIS POR MENOS'
END) AS offer_type_view,
(CASE WHEN pat.id_unique = 'price_group' THEN pa.id ELSE NULL END) AS 'group_price_attribute_id',
(CASE WHEN pat.id_unique = 'price_group' THEN pa.name ELSE NULL END) AS 'group_price_attribute_name'
FROM offer_products op
INNER JOIN products AS p ON p.id = op.product_id
LEFT JOIN product_attributes_products pap ON pap.product_id = op.product_id
LEFT JOIN product_attributes pa ON pa.id = pap.attribute_id
LEFT JOIN product_attributes_types pat ON pat.id = pa.type_id
LEFT JOIN offer_products_motives AS om ON om.id = op.offer_motive_id
INNER JOIN users AS uadded ON uadded.id = op.added_by
INNER JOIN partners AS padded ON padded.id = uadded.partner_id
LEFT JOIN users AS uupdated ON uupdated.id = op.updated_by
LEFT JOIN partners AS pupdated ON pupdated.id = uupdated.partner_id
GROUP BY op.id
