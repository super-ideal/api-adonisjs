CREATE OR REPLACE VIEW partners_view AS
SELECT p.*,
pe.email, pe.description email_description, pe.type email_type, pe.id email_id,
(pp.ddi + pp.ddd + pp.number) phone_number, pp.telephone_extension phone_extension, pp.description phone_description, pp.type phone_type, pp.id phone_id,
pa.id address_id, pa.cep address_cep, pa.street address_street, pa.number address_number, pa.complement address_complement, pa.neighborhood address_neighborhood, pa.city address_city, pa.state address_state
FROM partners AS p
LEFT JOIN partner_emails as pe on pe.partner_id = p.id
LEFT JOIN partner_phones as pp on pp.partner_id = p.id
LEFT JOIN partner_adresses as pa on pa.partner_id = p.id
