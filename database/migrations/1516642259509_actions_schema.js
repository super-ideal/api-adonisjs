'use strict'

const Schema = use('Schema')

class ActionsSchema extends Schema {
  static get connection () {
    return 'mysql'
  }

  up () {
    this.create('acl_actions', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('name', 80).notNullable()
      table.string('description').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('acl_actions')
  }
}

module.exports = ActionsSchema
