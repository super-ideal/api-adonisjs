'use strict'

const Schema = use('Schema')

class ProductSectionSchema extends Schema {
  up () {
    this.create('product_section', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('parent_id').unsigned().references('id').inTable('product_section')
      table.boolean('active').default(true).notNullable()
      table.string('name', 80).notNullable().unique()
      table.string('description')
      table.integer('type_id').unsigned().references('id').inTable('product_section_types').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_section')
  }
}

module.exports = ProductSectionSchema
