'use strict'

const Schema = use('Schema')

class ImagesSchema extends Schema {
  up () {
    this.create('product_images', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('filename').notNullable().unique()
      table.string('fieldname').notNullable()
      table.boolean('master').defaultTo(false)
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_images')
  }
}

module.exports = ImagesSchema
