'use strict'

const Schema = use('Schema')

class PartnerPhonesSchema extends Schema {
  up () {
    this.create('partner_phones', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('ddi', 4)
      table.integer('ddd', 3).notNullable()
      table.bigInteger('number', 11).notNullable()
      table.integer('telephone_extension', 4)
      table.string('description')
      table.string('type', 50).notNullable()
      table.integer('partner_id').unsigned().references('id').inTable('partners').notNullable()
      table.integer('added_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.unique(['number', 'ddd'])
      table.timestamps()
    })
  }

  down () {
    this.drop('partner_phones')
  }
}

module.exports = PartnerPhonesSchema
