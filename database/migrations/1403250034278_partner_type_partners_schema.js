'use strict'

const Schema = use('Schema')

class PartnerTypePartnerSchema extends Schema {
  up () {
    this.create('partner_types_partner', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('partner_id').unsigned().references('id').inTable('partners').notNullable()
      table.integer('partner_type_id').unsigned().references('id').inTable('partner_types').notNullable()
      table.unique(['partner_id', 'partner_type_id'])
    })
  }

  down () {
    this.drop('partner_types_partner')
  }
}

module.exports = PartnerTypePartnerSchema
