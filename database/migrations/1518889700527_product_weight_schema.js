'use strict'

const Schema = use('Schema')

class ProductWeightSchema extends Schema {
  up () {
    this.create('product_weights', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.decimal('approximate_weight').unsigned().comment('Peso aproximado do produto')
      table.decimal('capacity').unsigned().comment('Capacidade que o produto suporta')
      table.decimal('gross_weight').unsigned().comment('Peso bruto')
      table.decimal('net_weight').unsigned().comment('Peso líquido')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_weights')
  }
}

module.exports = ProductWeightSchema
