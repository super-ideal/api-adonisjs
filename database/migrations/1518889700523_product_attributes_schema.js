'use strict'

const Schema = use('Schema')

class ProductAttributesSchema extends Schema {
  up () {
    this.create('product_attributes', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').default(true).notNullable()
      table.string('name', 80).notNullable().unique()
      table.string('description')
      table.integer('type_id').unsigned().references('id').inTable('product_attributes_types').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_attributes')
  }
}

module.exports = ProductAttributesSchema
