'use strict'

const Schema = use('Schema')

class AuthSocialSchema extends Schema {
  up () {
    this.create('auth_socials', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable()
      table.string('name')
      table.string('avatar')
      table.string('username', 80)
      table.string('email', 254)
      table.string('provider_id').notNullable()
      table.string('provider', 10).notNullable()
      table.string('access_token')
      table.string('refresh_token')
      table.unique(['provider_id', 'provider'])
      table.timestamps()
    })
  }

  down () {
    this.drop('auth_socials')
  }
}

module.exports = AuthSocialSchema
