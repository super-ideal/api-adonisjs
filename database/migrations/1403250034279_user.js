'use strict'
const Schema = use('Schema')

class UserSchema extends Schema {
  static get connection () {
    return 'mysql'
  }

  up () {
    this.create('users', table => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('partner_id').unsigned().references('id').inTable('partners').unique()
      table.boolean('active').default(true).notNullable()
      table.string('username', 50).nullable().unique()
      table.string('email').notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('status', 50).notNullable().comment('Define o status do usuário (user, admin)').default('user')
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
