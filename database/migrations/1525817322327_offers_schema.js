'use strict'

const Schema = use('Schema')

class OffersSchema extends Schema {
  up () {
    this.create('offers', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').default(true).notNullable()
      table.string('name', 50).notNullable()
      table.string('description')
      table.integer('offer_types_id').unsigned().references('id').inTable('offer_types').notNullable()
      table.timestamp('datetime_start').notNullable().defaultTo(this.fn.now());
      table.timestamp('datetime_end').notNullable().defaultTo(this.fn.now());
      table.integer('created_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('offers')
  }
}

module.exports = OffersSchema
