'use strict'

const Schema = use('Schema')

class ShoppingListProductsSchema extends Schema {
  up () {
    this.create('shopping_list_products', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.decimal('quantity').unsigned().comment('Informa a quantida à comprar')
      table.decimal('quantity_purchased').unsigned().comment('Informa a quantidade comprada')
      table.timestamp('purchase_date').comment('Informa a data da compra do produto')
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.integer('list_id').unsigned().references('id').inTable('shopping_lists').notNullable()
      table.unique(['product_id', 'list_id'])
      table.timestamps()
    })
  }

  down () {
    this.drop('shopping_list_products')
  }
}

module.exports = ShoppingListProductsSchema
