'use strict'
const Schema = use('Schema')

class PagesSchema extends Schema {
  static get connection () {
    return 'mysql'
  }

  up () {
    this.create('acl_pages', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('name', 80).notNullable()
      table.string('description').notNullable()
      table.integer('tree').unsigned().references('id').inTable('acl_pages').onDelete('CASCADE')
      table.boolean('level')
      table.timestamps()
    })
  }

  down () {
    this.drop('acl_pages')
  }
}

module.exports = PagesSchema
