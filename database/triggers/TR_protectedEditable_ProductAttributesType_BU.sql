DROP TRIGGER IF EXISTS TR_protectedEditable_ProductAttributesType_BU;

DELIMITER $$
	CREATE TRIGGER TR_protectedEditable_ProductAttributesType_BU BEFORE UPDATE ON product_attributes_types
	FOR EACH ROW BEGIN
		IF (old.editable = 0) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração nesse dado.';
		END IF;
	END$$
DELIMITER ;