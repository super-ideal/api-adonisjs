DROP TRIGGER IF EXISTS TR_offer_products_BD;

DELIMITER $$
CREATE TRIGGER TR_offer_products_BD
BEFORE DELETE
	ON offer_products FOR EACH ROW
BEGIN

	/* Protege contra exclusão quando a agenda já finalizou */
	IF ((SELECT COUNT(*) FROM offers WHERE datetime_end < NOW() AND id = OLD.offer_id) != 0) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida exclusão de um item da agenda quando já foi finalizada.';
	/* Protege contra exclusão quando a agenda já iniciou */
	ELSEIF ((SELECT COUNT(*) FROM offers WHERE datetime_start <= NOW() AND id = OLD.offer_id) != 0) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida exclusão de um item da agenda quando já foi iniciada.';
	END IF;

END$$
DELIMITER ;
