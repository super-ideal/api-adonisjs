DROP TRIGGER IF EXISTS TR_offer_products_BI;

DELIMITER $$
CREATE TRIGGER TR_offer_products_BI
BEFORE INSERT
	ON offer_products FOR EACH ROW
BEGIN

	/* Protege contra inserção quando a agenda já finalizou */
	IF ((SELECT COUNT(*) FROM offers WHERE datetime_end < NOW() AND id = NEW.offer_id) != 0) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida adicionar um produto em uma agenda que já finalizou.';
	END IF;

END$$
DELIMITER ;
