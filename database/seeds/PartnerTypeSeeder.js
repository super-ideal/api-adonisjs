'use strict'

/*
|--------------------------------------------------------------------------
| PartnerTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class PartnerTypeSeeder {
  async run () {
    const model = [
      'Cliente',
      'Funcionário',
      'Fornecedor',
      'Transportador',
      'Convênio',
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Partner/PartnerType').create({ name: entry })
    }
  }
}

module.exports = PartnerTypeSeeder
