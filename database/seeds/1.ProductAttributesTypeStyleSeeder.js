'use strict'

/*
|--------------------------------------------------------------------------
| ProductAttributesTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class ProductAttributesTypeStyleSeeder {
  async run() {
    const model = [
      { id: 'dropdown', name: 'Menu suspenso (único)', description: 'Apresenta um menu suspenso para o usuário selecionar um atributo.' },
      { id: 'multiselect', name: 'Menu suspenso (vários)', description: 'Apresenta um menu suspenso para o usuário selecionar um ou mais atributos.' },
      { id: 'switch', name: 'Interruptor', description: 'Apresenta um botão com opção para marcar SIM ou NÃO.' },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Product/Attributes/AttributesTypeStyle').create({ data: entry })
    }
  }
}

module.exports = ProductAttributesTypeStyleSeeder
