'use strict'

/*
|--------------------------------------------------------------------------
| ProductSectionTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class ProductSectionTypeSeeder {
  async run() {
    const model = [
      { editable: 0, required: 1, name: 'Categoria', id_unique: 'category', parent_id: null},
      { editable: 0, required: 1, name: 'Subcategoria', id_unique: 'subcategory', parent_id: 1},
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Product/Section/SectionType').create({ data: entry })
    }
  }
}

module.exports = ProductSectionTypeSeeder
