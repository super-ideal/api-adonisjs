'use strict'

/*
|--------------------------------------------------------------------------
| OfferTypesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class OfferTypesSeeder {
  async run() {
    const model = [
      { active: 1, id_unique: 'cumulative', editable: 0, name: 'Acumulativo', opt_top_offer: 1, opt_discount: 1, opt_cumulative: 1, opt_wholesale: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
      { active: 1, id_unique: 'discount', editable: 0, name: 'Desconto', opt_top_offer: 1, opt_discount: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
      { active: 1, id_unique: 'take_more_pay_less', editable: 0, name: 'Leve Mais Pague Menos', opt_top_offer: 1, opt_cumulative: 1, opt_cumulative_payable: 1, opt_wholesale: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
      { active: 1, id_unique: 'two_price', editable: 0, name: 'Preço Dois', opt_top_offer: 1, opt_discount: 1, opt_discount_price_two: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
      { active: 1, id_unique: null, editable: 1, name: 'Atacado', opt_top_offer: 1, opt_discount: 1, opt_cumulative: 1, opt_wholesale: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
      { active: 1, id_unique: null, editable: 1, name: 'Scantech', opt_top_offer: 1, opt_discount: 1, opt_discount_price_two: 1, opt_cumulative: 1, opt_cumulative_payable: 1, opt_wholesale: 1, opt_price_group: 1, opt_limit_per_coupon: 1, opt_view_percent_discount: 1 },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Offer/OfferType').create({ data: entry })
    }
  }
}

module.exports = OfferTypesSeeder
